--------------------------------------------------------------------------------
-- Queries to create dims already in the warehouse
--------------------------------------------------------------------------------

-- Create schema for the warehouse
CREATE DATABASE warehouse;

-- Queries 1-D-1: Create and populate dim_student
CREATE TABLE warehouse.dim_student
    (dim_student_key decimal(20),
     pidm decimal(8),
     username varchar(20),
     first_name varchar(60),
     last_name varchar(60),
     first_gen varchar(1));

INSERT INTO warehouse.dim_student VALUES ('1', '519353', 'AAROLS28', 'Aaron', 'Olsen', 'U');
INSERT INTO warehouse.dim_student VALUES ('2', '546878', 'JOHWRI56', 'John', 'Wright', 'Y');
INSERT INTO warehouse.dim_student VALUES ('3', '269787', 'KEVJON36', 'Kevin', 'Jones', 'Y');
INSERT INTO warehouse.dim_student VALUES ('4', '752453', 'PAUTHO49', 'Paul', 'Thomas', 'N');
INSERT INTO warehouse.dim_student VALUES ('5', '137588', 'JUSSAN82', 'Justin', 'Sanchez', 'Y');
INSERT INTO warehouse.dim_student VALUES ('6', '215090', 'EMISCO77', 'Emily', 'Scott', 'Y');
INSERT INTO warehouse.dim_student VALUES ('7', '178294', 'MELDAV87', 'Melissa', 'Davis', 'Y');
INSERT INTO warehouse.dim_student VALUES ('8', '159552', 'LISLOP66', 'Lisa', 'Lopez', 'N');
INSERT INTO warehouse.dim_student VALUES ('9', '776269', 'GARROB86', 'Gary', 'Roberts', 'U');
INSERT INTO warehouse.dim_student VALUES ('10', '694416', 'ASHROD95', 'Ashley', 'Rodriguez', 'Y');
INSERT INTO warehouse.dim_student VALUES ('11', '638159', 'KIMEVA61', 'Kimberly', 'Evans', 'N');
INSERT INTO warehouse.dim_student VALUES ('12', '434067', 'MEGYOU97', 'Megan', 'Young', 'Y');
INSERT INTO warehouse.dim_student VALUES ('13', '548987', 'JENPAR18', 'Jennifer', 'Parker', 'Y');
INSERT INTO warehouse.dim_student VALUES ('14', '185262', 'ERIRAS42', 'Eric', 'Rasmussen', 'U');
INSERT INTO warehouse.dim_student VALUES ('15', '511449', 'KENSMI62', 'Kenneth', 'Smith', 'N');
INSERT INTO warehouse.dim_student VALUES ('16', '983156', 'JORNIE28', 'Jordan', 'Nielson', 'U');
INSERT INTO warehouse.dim_student VALUES ('17', '624369', 'ANDHAR29', 'Andrew', 'Harris', 'U');
INSERT INTO warehouse.dim_student VALUES ('18', '662700', 'LAUPET71', 'Laura', 'Peterson', 'Y');
INSERT INTO warehouse.dim_student VALUES ('19', '558146', 'ROBGAR43', 'Robert', 'Gardner', 'N');
INSERT INTO warehouse.dim_student VALUES ('20', '793005', 'JESBRO89', 'Jessica', 'Brown', 'N');
INSERT INTO warehouse.dim_student VALUES ('21', '248168', 'ANTLEE67', 'Anthony', 'Lee', 'Y');
INSERT INTO warehouse.dim_student VALUES ('22', '147704', 'AMYTUR12', 'Amy', 'Turner', 'Y');
INSERT INTO warehouse.dim_student VALUES ('23', '980097', 'MICCAR11', 'Michelle', 'Carter', 'U');
INSERT INTO warehouse.dim_student VALUES ('24', '249358', 'KARNGU99', 'Karen', 'Nguyen', 'N');
INSERT INTO warehouse.dim_student VALUES ('25', '504093', 'JARKIN70', 'Jared', 'King', 'N');
INSERT INTO warehouse.dim_student VALUES ('26', '972806', 'MATHAL83', 'Matthew', 'Hall', 'N');
INSERT INTO warehouse.dim_student VALUES ('27', '620401', 'AMAGRE95', 'Amanda', 'Green', 'N');
INSERT INTO warehouse.dim_student VALUES ('28', '540514', 'JOSCOO10', 'Joseph', 'Cook', 'Y');
INSERT INTO warehouse.dim_student VALUES ('29', '347288', 'JACJOH60', 'Jacob', 'Johnson', 'N');
INSERT INTO warehouse.dim_student VALUES ('30', '250998', 'JEFBAK78', 'Jeffrey', 'Baker', 'U');
INSERT INTO warehouse.dim_student VALUES ('31', '908154', 'JULTAY95', 'Julie', 'Taylor', 'N');
INSERT INTO warehouse.dim_student VALUES ('32', '139291', 'NICMAR39', 'Nicole', 'Martinez', 'N');
INSERT INTO warehouse.dim_student VALUES ('33', '237328', 'SARWAL78', 'Sarah', 'Walker', 'Y');
INSERT INTO warehouse.dim_student VALUES ('34', '831225', 'DANNEL30', 'Daniel', 'Nelson', 'U');
INSERT INTO warehouse.dim_student VALUES ('35', '963562', 'JONNIE32', 'Jonathan', 'Nielsen', 'Y');
INSERT INTO warehouse.dim_student VALUES ('36', '599321', 'NATSOR23', 'Nathan', 'Sorensen', 'Y');
INSERT INTO warehouse.dim_student VALUES ('37', '360227', 'JERLAR37', 'Jeremy', 'Larsen', 'U');
INSERT INTO warehouse.dim_student VALUES ('38', '902169', 'MARCLA98', 'Mark', 'Clark', 'Y');
INSERT INTO warehouse.dim_student VALUES ('39', '559531', 'JASPET31', 'Jason', 'Petersen', 'U');
INSERT INTO warehouse.dim_student VALUES ('40', '523397', 'HEASTE72', 'Heather', 'Stewart', 'U');
INSERT INTO warehouse.dim_student VALUES ('41', '368243', 'RICALL65', 'Richard', 'Allred', 'U');
INSERT INTO warehouse.dim_student VALUES ('42', '754974', 'BRAMIT87', 'Brandon', 'Mitchell', 'U');
INSERT INTO warehouse.dim_student VALUES ('43', '470456', 'MICROB45', 'Michael', 'Robinson', 'Y');
INSERT INTO warehouse.dim_student VALUES ('44', '600629', 'CHAMOO40', 'Charles', 'Moore', 'U');
INSERT INTO warehouse.dim_student VALUES ('45', '470899', 'STEJEN75', 'Steven', 'Jensen', 'N');
INSERT INTO warehouse.dim_student VALUES ('46', '645969', 'WILHIL20', 'William', 'Hill', 'N');
INSERT INTO warehouse.dim_student VALUES ('47', '329228', 'ADAWOO38', 'Adam', 'Wood', 'Y');
INSERT INTO warehouse.dim_student VALUES ('48', '315061', 'REBAND60', 'Rebecca', 'Anderson', 'Y');
INSERT INTO warehouse.dim_student VALUES ('49', '150722', 'JOSCAM20', 'Joshua', 'Campbell', 'U');
INSERT INTO warehouse.dim_student VALUES ('50', '212694', 'JAMLEW69', 'James', 'Lewis', 'N');

-- Queries 1-D-2: Create and populate dim_section
CREATE TABLE warehouse.dim_section
    (dim_section_key decimal(20),
     term_code varchar(6),
     crn varchar(5),
     course_subj varchar(4),
     course_num varchar(5),
     section_num varchar(3));

INSERT INTO warehouse.dim_section VALUES ('1', '201920', '80141', 'ENGL', '101', '01');
INSERT INTO warehouse.dim_section VALUES ('2', '201920', '57815', 'ENGL', '201', '01');
INSERT INTO warehouse.dim_section VALUES ('3', '201920', '61490', 'MATH', '101', '01');
INSERT INTO warehouse.dim_section VALUES ('4', '201920', '25487', 'MATH', '201', '01');
INSERT INTO warehouse.dim_section VALUES ('5', '201920', '25763', 'HIST', '101', '01');
INSERT INTO warehouse.dim_section VALUES ('6', '201920', '77786', 'ENGL', '101', '02');
INSERT INTO warehouse.dim_section VALUES ('7', '201920', '82239', 'ENGL', '201', '02');
INSERT INTO warehouse.dim_section VALUES ('8', '201920', '94456', 'MATH', '101', '02');
INSERT INTO warehouse.dim_section VALUES ('9', '201920', '30084', 'MATH', '201', '02');
INSERT INTO warehouse.dim_section VALUES ('10', '201920', '63064', 'HIST', '101', '02');
INSERT INTO warehouse.dim_section VALUES ('11', '201920', '95507', 'ENGL', '101', '03');
INSERT INTO warehouse.dim_section VALUES ('12', '201920', '19183', 'ENGL', '201', '03');
INSERT INTO warehouse.dim_section VALUES ('13', '201920', '53455', 'MATH', '101', '03');
INSERT INTO warehouse.dim_section VALUES ('14', '201920', '44740', 'MATH', '201', '03');
INSERT INTO warehouse.dim_section VALUES ('15', '201920', '90118', 'HIST', '101', '03');

-- Queries 1-D-3: Create and populate dim_instructor
CREATE TABLE warehouse.dim_instructor
    (dim_instructor_key decimal(20),
     pidm decimal(8),
     username varchar(20),
     first_name varchar(60),
     last_name varchar(60));

INSERT INTO warehouse.dim_instructor VALUES ('51', '391642', 'CHRADA97', 'Christopher', 'Adams');
INSERT INTO warehouse.dim_instructor VALUES ('52', '220550', 'RYAMIL32', 'Ryan', 'Miller');
INSERT INTO warehouse.dim_instructor VALUES ('53', '326782', 'THOJAC85', 'Thomas', 'Jackson');
INSERT INTO warehouse.dim_instructor VALUES ('54', '936100', 'ANGHER77', 'Angela', 'Hernandez');
INSERT INTO warehouse.dim_instructor VALUES ('55', '154682', 'SCOWIL50', 'Scott', 'Wilson');
INSERT INTO warehouse.dim_instructor VALUES ('56', '828952', 'BRIGAR24', 'Brian', 'Garcia');
INSERT INTO warehouse.dim_instructor VALUES ('57', '314061', 'MARWHI55', 'Mary', 'White');
INSERT INTO warehouse.dim_instructor VALUES ('58', '998727', 'BENGON54', 'Benjamin', 'Gonzalez');
INSERT INTO warehouse.dim_instructor VALUES ('59', '467393', 'TYLALL65', 'Tyler', 'Allen');
INSERT INTO warehouse.dim_instructor VALUES ('60', '310713', 'STECHR71', 'Stephanie', 'Christensen');
INSERT INTO warehouse.dim_instructor VALUES ('61', '496740', 'NICHAN41', 'Nicholas', 'Hansen');
INSERT INTO warehouse.dim_instructor VALUES ('62', '748185', 'RACBLA36', 'Rachel', 'Black');
INSERT INTO warehouse.dim_instructor VALUES ('63', '862308', 'DAVMAR40', 'David', 'Martin');
INSERT INTO warehouse.dim_instructor VALUES ('64', '484009', 'SUSWIL45', 'Susan', 'Williams');
INSERT INTO warehouse.dim_instructor VALUES ('65', '211604', 'ELITHO85', 'Elizabeth', 'Thompson');

