--------------------------------------------------------------------------------
-- Queries to create load tables for raw Canvas data (and dummy Banner tables)
--------------------------------------------------------------------------------

-- Create schemas for Canvas and Banner
CREATE DATABASE canvas;
CREATE DATABASE banner;

-- Query 1-A-1: Create pseudonym_dim
CREATE TABLE canvas.pseudonym_dim
    (id decimal(20),
     canvas_id decimal(20),
     user_id decimal(20),
     account_id decimal(20),
     workflow_state varchar(20),
     last_request_at timestamp,
     last_login_at timestamp,
     current_login_at timestamp,
     last_login_ip varchar(64),
     current_login_ip varchar(64),
     position decimal(10),
     created_at timestamp,
     updated_at timestamp,
     password_auto_generated varchar(10),
     deleted_at timestamp,
     sis_user_id varchar(64),
     unique_name varchar(64),
     integration_id varchar(64),
     authentication_provider_id decimal(20));

-- Query 1-A-2: Create user_dim
CREATE TABLE canvas.user_dim
    (id decimal(20),
     canvas_id decimal(20),
     root_account_id decimal(20),
     name varchar(64),
     time_zone varchar(64),
     created_at timestamp,
     visibility varchar(64),
     school_name varchar(64),
     school_position varchar(64),
     gender varchar(64),
     locale varchar(20),
     is_public varchar(10),
     birthdate varchar(64),
     country_code varchar(64),
     workflow_state varchar(20),
     sortable_name varchar(64),
     global_canvas_id decimal(20));

-- Query 1-A-3: Create course_dim
CREATE TABLE canvas.course_dim
    (id decimal(20),
     canvas_id decimal(20),
     root_account_id decimal(20),
     account_id decimal(20),
     enrollment_term_id decimal(20),
     name varchar(100),
     code varchar(100),
     type varchar(10),
     created_at timestamp,
     start_at timestamp,
     conclude_at timestamp,
     publicly_visible varchar(30),
     sis_source_id varchar(2000),
     workflow_state varchar(20),
     wiki_id decimal(20),
     syllabus_body varchar(4000));

-- Query 1-A-4: Create course_section_dim
CREATE TABLE canvas.course_section_dim
    (id decimal(20),
     canvas_id decimal(20),
     name varchar(100),
     course_id decimal(20),
     enrollment_term_id decimal(20),
     default_section varchar(10),
     accepting_enrollments varchar(10),
     can_manually_enroll varchar(10),
     start_at timestamp,
     end_at timestamp,
     created_at timestamp,
     updated_at timestamp,
     workflow_state varchar(20),
     restrict_enrollments_to_section_dates varchar(10),
     nonxlist_course_id decimal(20),
     sis_source_id varchar(64));

-- Query 1-A-5: Create assignment_dim
CREATE TABLE canvas.assignment_dim
    (id decimal(20),
     canvas_id decimal(20),
     course_id decimal(20),
     title varchar(4000),
     description varchar(4000),
     due_at timestamp,
     unlock_at timestamp,
     lock_at timestamp,
     points_possible decimal(20),
     grading_type varchar(20),
     submission_types varchar(64),
     workflow_state varchar(20),
     created_at timestamp,
     updated_at timestamp,
     peer_review_count decimal(20),
     peer_reviews_due_at timestamp,
     peer_reviews_assigned varchar(10),
     peer_reviews varchar(10),
     automatic_peer_reviews varchar(10),
     all_day varchar(10),
     all_day_date timestamp,
     could_be_locked varchar(10),
     grade_group_students_individually varchar(10),
     anonymous_peer_reviews varchar(10),
     muted varchar(10),
     assignment_group_id decimal(20),
     position decimal(20),
     visibility varchar(64),
     external_tool_id decimal(20));

-- Query 1-A-6: Create assignment_group_dim
CREATE TABLE canvas.assignment_group_dim
    (id decimal(20),
     canvas_id decimal(20),
     course_id decimal(20),
     name varchar(4000),
     default_assignment_name varchar(4000),
     workflow_state varchar(20),
     position decimal(20),
     created_at timestamp,
     updated_at timestamp);

-- Query 1-A-7: Create assignment_group_fact
CREATE TABLE canvas.assignment_group_fact
    (assignment_group_id decimal(20),
     course_id decimal(20),
     group_weight decimal(20));

-- Query 1-A-8: Create submission_dim
CREATE TABLE canvas.submission_dim
    (id decimal(20),
     canvas_id decimal(20),
     body varchar(4000),
     url varchar(4000),
     grade varchar(70),
     submitted_at timestamp,
     submission_type varchar(30),
     workflow_state varchar(20),
     created_at timestamp,
     updated_at timestamp,
     processed varchar(10),
     process_attempts decimal(20),
     grade_matches_current_submission varchar(10),
     published_grade varchar(70),
     graded_at timestamp,
     has_rubric_assessment varchar(10),
     attempt decimal(20),
     has_admin_comment varchar(10),
     assignment_id decimal(20),
     excused varchar(30),
     graded_anonymously varchar(30),
     grader_id decimal(20),
     group_id decimal(20),
     quiz_submission_id decimal(20),
     user_id decimal(20),
     grade_state varchar(20));

-- Query 1-A-9: Create submission_fact
CREATE TABLE canvas.submission_fact
    (submission_id decimal(20),
     assignment_id decimal(20),
     course_id decimal(20),
     enrollment_term_id decimal(20),
     user_id decimal(20),
     grader_id decimal(20),
     course_account_id decimal(20),
     enrollment_rollup_id decimal(20),
     score decimal(20),
     published_score decimal(20),
     what_if_score decimal(20),
     submission_comments_count decimal(20),
     account_id decimal(20),
     assignment_group_id decimal(20),
     group_id decimal(20),
     quiz_id decimal(20),
     quiz_submission_id decimal(20),
     wiki_id decimal(20));

-- Query 1-A-10: Create gobsrid
CREATE TABLE banner.gobsrid
    (gobsrid_pidm decimal(8),
     gobsrid_sourced_id varchar(16));

-- Query 1-A-11: Create goremal
CREATE TABLE banner.goremal
    (goremal_pidm decimal(8),
     goremal_email_address varchar(128));

-- Query 1-A-12: Create ssbsect
CREATE TABLE banner.ssbsect
    (ssbsect_term_code varchar(6),
     ssbsect_crn varchar(5),
     ssbsect_subj_code varchar(4),
     ssbsect_crse_numb varchar(5),
     ssbsect_seq_numb varchar(3));

-- Query 1-A-13: Create sfrstcr
CREATE TABLE banner.sfrstcr
    (sfrstcr_term_code varchar(6),
     sfrstcr_pidm decimal(8),
     sfrstcr_crn varchar(5),
     sfrstcr_rsts_code varchar(2),
     sfrstcr_grde_code varchar(6));

