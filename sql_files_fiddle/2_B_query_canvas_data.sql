--------------------------------------------------------------------------------
-- Queries to query the Canvas data in the new star schemas
--------------------------------------------------------------------------------

-- Query 2-B-1: What are the final exam scores of all students in MATH 101?
SELECT dim_section.course_subj, dim_section.course_num, dim_section.section_num,
       CONCAT(dim_instructor.first_name, CONCAT(' ', dim_instructor.last_name)) AS instructor_name,
       dim_student.pidm,
       CONCAT(dim_student.first_name, CONCAT(' ', dim_student.last_name)) AS student_name,
       dim_assignment.title, fact_assgn_submissions.grade
FROM warehouse.fact_assgn_submissions
     JOIN warehouse.dim_assignment
     ON fact_assgn_submissions.dim_assignment_key = dim_assignment.dim_assignment_key
     JOIN warehouse.dim_student
     ON fact_assgn_submissions.dim_student_key = dim_student.dim_student_key
     JOIN warehouse.dim_section
     ON fact_assgn_submissions.dim_section_key = dim_section.dim_section_key
     JOIN warehouse.dim_instructor
     ON fact_assgn_submissions.dim_instructor_key = dim_instructor.dim_instructor_key
WHERE dim_section.course_subj = 'MATH'
      AND dim_section.course_num = '101'
      AND dim_assignment.TITLE = 'Assignment  6'
ORDER BY dim_section.SECTION_NUM, dim_student.last_name;

-- Query 2-B-2: Are first-generation students any more (or less) likely to
-- submit assignments?
SELECT dim_student.first_gen,
       ROUND(SUM(CASE WHEN fact_assgn_submissions.date_submitted IS NOT NULL THEN 1 ELSE 0 END) / COUNT(*) * 100, 2) AS PCT_SUBMITTED
FROM warehouse.fact_assgn_submissions
     JOIN warehouse.dim_student
     ON fact_assgn_submissions.dim_student_key = dim_student.dim_student_key
GROUP BY dim_student.first_gen;