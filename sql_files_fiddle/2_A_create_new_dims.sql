--------------------------------------------------------------------------------
-- Queries to create new dims from Canvas data
--------------------------------------------------------------------------------

-- Queries 2-A-1: Create dim_assignment
CREATE TABLE warehouse.dim_assignment (dim_assignment_key int AUTO_INCREMENT PRIMARY KEY) AS
    (SELECT -- Get the Canvas assignment and course IDs; these are our business
            -- keys.
            assignment_dim.canvas_id,
            assignment_dim.id AS canvas_internal_id,
            assignment_dim.course_id AS canvas_course_id,
            -- Title of the assignment.
            assignment_dim.title,
            -- Created date and due date of the assignment.
            assignment_dim.created_at AS date_created,
            assignment_dim.due_at AS date_due,
            -- How many points is the assignment worth?
            assignment_dim.points_possible,
            -- ROUGHLY what percent of the student's overall grade is the
            -- assignment worth?
            (assignment_dim.points_possible /
             SUM(assignment_dim.points_possible)
             OVER (PARTITION BY assignment_dim.assignment_group_id)) *
            (assignment_group_fact.group_weight /
             assignment_group_fact.total_group_weights) * 100 AS percent_worth,
            -- What kind of grade does this assignment receive?  (points,
            -- complete/incomplete, etc.)
            assignment_dim.grading_type,
            -- How do students submit this assignment?
            assignment_dim.submission_types,
            -- Has this assignment been published?
            assignment_dim.workflow_state,
            -- What group is this assignment in?
            assignment_group_dim.id AS canvas_assignment_group_id,
            assignment_group_dim.name AS assignment_group
     FROM -- Assignment-level fields.
          canvas.assignment_dim
          -- Join to assignment_group_dim to get the name of the group.
          JOIN canvas.assignment_group_dim
          ON assignment_dim.ASSIGNMENT_GROUP_ID = assignment_group_dim.ID
          -- From assignment_group_fact, get the weight of each group and the
          -- total of all group weights in the course (NOT always 100% in real
          -- Canvas data).
          JOIN (SELECT assignment_group_id, group_weight,
                       SUM(group_weight)
                       OVER (PARTITION BY course_id) AS total_group_weights
                FROM canvas.assignment_group_fact) assignment_group_fact
          ON assignment_dim.ASSIGNMENT_GROUP_ID = assignment_group_fact.ASSIGNMENT_GROUP_ID);

-- Query 2-A-2: Create fact_assgn_submissions
CREATE TABLE warehouse.fact_assgn_submissions AS
    (SELECT -- Get dimension keys.
            dim_student.dim_student_key,
            dim_instructor.dim_instructor_key,
            dim_assignment.dim_assignment_key,
            dim_section.dim_section_key,
            -- Has this submission been submitted/graded yet?  When?
            submission_dim.workflow_state,
            submission_dim.submitted_at as date_submitted,
            submission_dim.graded_at as date_graded,
            -- Grade of the submission.
            submission_dim.grade,
            submission_fact.score
     FROM -- Some submission-level fields.
          canvas.submission_dim
          -- More submission-level fields.
          JOIN canvas.submission_fact
          ON submission_dim.id = submission_fact.submission_id
          -- Get the student via USER_ID -> pseudonym_dim -> gobsrid ->
          -- dim_student.
          JOIN canvas.pseudonym_dim pseudonym_student
          ON submission_dim.user_id = pseudonym_student.user_id
          JOIN banner.gobsrid gobsrid_student
          ON pseudonym_student.sis_user_id = gobsrid_student.gobsrid_sourced_id
          JOIN warehouse.dim_student
          ON gobsrid_student.gobsrid_pidm = dim_student.pidm
          -- Get the grader via GRADER_ID -> pseudonym_dim -> gobsrid ->
          -- dim_instructor.
          JOIN canvas.pseudonym_dim pseudonym_instructor
          ON submission_dim.grader_id = pseudonym_instructor.user_id
          JOIN banner.gobsrid gobsrid_instructor
          ON pseudonym_instructor.sis_user_id = gobsrid_instructor.gobsrid_sourced_id
          JOIN warehouse.dim_instructor
          ON gobsrid_instructor.gobsrid_pidm = dim_instructor.pidm
          -- Get the course section.
          JOIN canvas.course_section_dim
          ON submission_fact.course_id = course_section_dim.course_id
          JOIN banner.sfrstcr
          ON SUBSTR(course_section_dim.sis_source_id, 1, 5) = sfrstcr.sfrstcr_crn
             AND SUBSTR(course_section_dim.sis_source_id, 7, 12) = sfrstcr.sfrstcr_term_code
             AND dim_student.pidm = sfrstcr.sfrstcr_pidm
          JOIN warehouse.dim_section
          ON sfrstcr.sfrstcr_crn = dim_section.crn
             AND sfrstcr.sfrstcr_term_code = dim_section.term_code
          -- Get the assignment.
          JOIN warehouse.dim_assignment
          ON submission_dim.assignment_id = dim_assignment.canvas_internal_id);