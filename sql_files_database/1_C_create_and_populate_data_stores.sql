--------------------------------------------------------------------------------
-- Queries to create and populate data stores for raw Canvas data
--------------------------------------------------------------------------------

-- Queries 1-C-1: Create and populate canvas_pseudonym_dim
CREATE TABLE canvas_pseudonym_dim
    (id decimal(20),
     canvas_id decimal(20),
     user_id decimal(20),
     account_id decimal(20),
     workflow_state varchar(20),
     last_request_at timestamp,
     last_login_at timestamp,
     current_login_at timestamp,
     last_login_ip varchar(64),
     current_login_ip varchar(64),
     position decimal(10),
     created_at timestamp,
     updated_at timestamp,
     password_auto_generated varchar(10),
     deleted_at timestamp,
     sis_user_id varchar(64),
     unique_name varchar(64),
     integration_id varchar(64),
     authentication_provider_id decimal(20));

INSERT INTO canvas_pseudonym_dim
    (SELECT CAST(NULLIF(id, '\N') AS decimal(20)),
            CAST(NULLIF(canvas_id, '\N') AS decimal(20)),
            CAST(NULLIF(user_id, '\N') AS decimal(20)),
            CAST(NULLIF(account_id, '\N') AS decimal(20)),
            NULLIF(workflow_state, '\N'),
            TO_TIMESTAMP(NULLIF(last_request_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            TO_TIMESTAMP(NULLIF(last_login_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            TO_TIMESTAMP(NULLIF(current_login_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            NULLIF(last_login_ip, '\N'),
            NULLIF(current_login_ip, '\N'),
            CAST(NULLIF(position, '\N') AS decimal(10)),
            TO_TIMESTAMP(NULLIF(created_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            TO_TIMESTAMP(NULLIF(updated_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            NULLIF(password_auto_generated, '\N'),
            TO_TIMESTAMP(NULLIF(deleted_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            NULLIF(sis_user_id, '\N'),
            NULLIF(unique_name, '\N'),
            NULLIF(integration_id, '\N'),
            CAST(NULLIF(authentication_provider_id, '\N') AS decimal(20))
     FROM canvas_load_pseudonym_dim);

-- Queries 1-C-2: Create and populate canvas_user_dim
CREATE TABLE canvas_user_dim
    (id decimal(20),
     canvas_id decimal(20),
     root_account_id decimal(20),
     name varchar(64),
     time_zone varchar(64),
     created_at timestamp,
     visibility varchar(64),
     school_name varchar(64),
     school_position varchar(64),
     gender varchar(64),
     locale varchar(20),
     is_public varchar(10),
     birthdate varchar(64),
     country_code varchar(64),
     workflow_state varchar(20),
     sortable_name varchar(64),
     global_canvas_id decimal(20));

INSERT INTO canvas_user_dim
    (SELECT CAST(NULLIF(id, '\N') AS decimal(20)),
            CAST(NULLIF(canvas_id, '\N') AS decimal(20)),
            CAST(NULLIF(root_account_id, '\N') AS decimal(20)),
            NULLIF(name, '\N'),
            NULLIF(time_zone, '\N'),
            TO_TIMESTAMP(NULLIF(created_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            NULLIF(visibility, '\N'),
            NULLIF(school_name, '\N'),
            NULLIF(school_position, '\N'),
            NULLIF(gender, '\N'),
            CAST(NULLIF(locale, '\N') AS varchar(20)),
            CAST(NULLIF(is_public, '\N') AS varchar(10)),
            NULLIF(birthdate, '\N'),
            NULLIF(country_code, '\N'),
            CAST(NULLIF(workflow_state, '\N') AS varchar(20)),
            NULLIF(sortable_name, '\N'),
            CAST(NULLIF(global_canvas_id, '\N') AS decimal(20))
     FROM canvas_load_user_dim);

-- Queries 1-C-3: Create and populate canvas_course_dim
CREATE TABLE canvas_course_dim
    (id decimal(20),
     canvas_id decimal(20),
     root_account_id decimal(20),
     account_id decimal(20),
     enrollment_term_id decimal(20),
     name varchar(100),
     code varchar(100),
     type varchar(10),
     created_at timestamp,
     start_at timestamp,
     conclude_at timestamp,
     publicly_visible varchar(30),
     sis_source_id varchar(2000),
     workflow_state varchar(20),
     wiki_id decimal(20),
     syllabus_body varchar(4000));

INSERT INTO canvas_course_dim
    (SELECT CAST(NULLIF(id, '\N') AS decimal(20)),
            CAST(NULLIF(canvas_id, '\N') AS decimal(20)),
            CAST(NULLIF(root_account_id, '\N') AS decimal(20)),
            CAST(NULLIF(account_id, '\N') AS decimal(20)),
            CAST(NULLIF(enrollment_term_id, '\N') AS decimal(20)),
            NULLIF(name, '\N'),
            NULLIF(code, '\N'),
            NULLIF(type, '\N'),
            TO_TIMESTAMP(NULLIF(created_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            TO_TIMESTAMP(NULLIF(start_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            TO_TIMESTAMP(NULLIF(conclude_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            NULLIF(publicly_visible, '\N'),
            NULLIF(sis_source_id, '\N'),
            NULLIF(workflow_state, '\N'),
            CAST(NULLIF(wiki_id, '\N') AS decimal(20)),
            syllabus_body
     FROM canvas_load_course_dim);

-- Queries 1-C-4: Create and populate canvas_course_section_dim
CREATE TABLE canvas_course_section_dim
    (id decimal(20),
     canvas_id decimal(20),
     name varchar(100),
     course_id decimal(20),
     enrollment_term_id decimal(20),
     default_section varchar(10),
     accepting_enrollments varchar(10),
     can_manually_enroll varchar(10),
     start_at timestamp,
     end_at timestamp,
     created_at timestamp,
     updated_at timestamp,
     workflow_state varchar(20),
     restrict_enrollments_to_section_dates varchar(10),
     nonxlist_course_id decimal(20),
     sis_source_id varchar(64));

INSERT INTO canvas_course_section_dim
    (SELECT CAST(NULLIF(id, '\N') AS decimal(20)),
            CAST(NULLIF(canvas_id, '\N') AS decimal(20)),
            NULLIF(name, '\N'),
            CAST(NULLIF(course_id, '\N') AS decimal(20)),
            CAST(NULLIF(enrollment_term_id, '\N') AS decimal(20)),
            NULLIF(default_section, '\N'),
            NULLIF(accepting_enrollments, '\N'),
            NULLIF(can_manually_enroll, '\N'),
            TO_TIMESTAMP(NULLIF(start_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            TO_TIMESTAMP(NULLIF(end_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            TO_TIMESTAMP(NULLIF(created_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            TO_TIMESTAMP(NULLIF(updated_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            NULLIF(workflow_state, '\N'),
            NULLIF(restrict_enrollments_to_section_dates, '\N'),
            CAST(NULLIF(nonxlist_course_id, '\N') AS decimal(20)),
            NULLIF(sis_source_id, '\N')
     FROM canvas_load_course_section_dim);

-- Queries 1-C-5: Create and populate canvas_assignment_dim
CREATE TABLE canvas_assignment_dim
    (id decimal(20),
     canvas_id decimal(20),
     course_id decimal(20),
     title varchar(4000),
     description varchar(4000),
     due_at timestamp,
     unlock_at timestamp,
     lock_at timestamp,
     points_possible decimal(20),
     grading_type varchar(20),
     submission_types varchar(64),
     workflow_state varchar(20),
     created_at timestamp,
     updated_at timestamp,
     peer_review_count decimal(20),
     peer_reviews_due_at timestamp,
     peer_reviews_assigned varchar(10),
     peer_reviews varchar(10),
     automatic_peer_reviews varchar(10),
     all_day varchar(10),
     all_day_date timestamp,
     could_be_locked varchar(10),
     grade_group_students_individually varchar(10),
     anonymous_peer_reviews varchar(10),
     muted varchar(10),
     assignment_group_id decimal(20),
     position decimal(20),
     visibility varchar(64),
     external_tool_id decimal(20));

INSERT INTO canvas_assignment_dim
    (SELECT CAST(NULLIF(id, '\N') AS decimal(20)),
            CAST(NULLIF(canvas_id, '\N') AS decimal(20)),
            CAST(NULLIF(course_id, '\N') AS decimal(20)),
            NULLIF(title, '\N'),
            NULLIF(description, '\N'),
            TO_TIMESTAMP(NULLIF(due_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            TO_TIMESTAMP(NULLIF(unlock_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            TO_TIMESTAMP(NULLIF(lock_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            CAST(NULLIF(points_possible, '\N') AS decimal(20)),
            NULLIF(grading_type, '\N'),
            NULLIF(submission_types, '\N'),
            NULLIF(workflow_state, '\N'),
            TO_TIMESTAMP(NULLIF(created_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            TO_TIMESTAMP(NULLIF(updated_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            CAST(NULLIF(peer_review_count, '\N') AS decimal(20)),
            TO_TIMESTAMP(NULLIF(peer_reviews_due_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            NULLIF(peer_reviews_assigned, '\N'),
            NULLIF(peer_reviews, '\N'),
            NULLIF(automatic_peer_reviews, '\N'),
            NULLIF(all_day, '\N'),
            TO_TIMESTAMP(NULLIF(all_day_date, '\N'), 'YYYY-MM-DD'),
            NULLIF(could_be_locked, '\N'),
            NULLIF(grade_group_students_individually, '\N'),
            NULLIF(anonymous_peer_reviews, '\N'),
            NULLIF(muted, '\N'),
            CAST(NULLIF(assignment_group_id, '\N') AS decimal(20)),
            CAST(NULLIF(position, '\N') AS decimal(20)),
            NULLIF(visibility, '\N'),
            CAST(NULLIF(external_tool_id, '\N') AS decimal(20))
     FROM canvas_load_assignment_dim);

-- Queries 1-C-6: Create and populate canvas_assignment_group_dim
CREATE TABLE canvas_assignment_group_dim
    (id decimal(20),
     canvas_id decimal(20),
     course_id decimal(20),
     name varchar(4000),
     default_assignment_name varchar(4000),
     workflow_state varchar(20),
     position decimal(20),
     created_at timestamp,
     updated_at timestamp);

INSERT INTO canvas_assignment_group_dim
    (SELECT CAST(NULLIF(id, '\N') AS decimal(20)),
            CAST(NULLIF(canvas_id, '\N') AS decimal(20)),
            CAST(NULLIF(course_id, '\N') AS decimal(20)),
            NULLIF(name, '\N'),
            NULLIF(default_assignment_name, '\N'),
            NULLIF(workflow_state, '\N'),
            CAST(NULLIF(position, '\N') AS decimal(20)),
            TO_TIMESTAMP(NULLIF(created_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            TO_TIMESTAMP(NULLIF(updated_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3')
     FROM canvas_load_assignment_group_dim);

-- Queries 1-C-7: Create and populate canvas_assignment_group_fact
CREATE TABLE canvas_assignment_group_fact
    (assignment_group_id decimal(20),
     course_id decimal(20),
     group_weight decimal(20));

INSERT INTO canvas_assignment_group_fact
    (SELECT CAST(NULLIF(assignment_group_id, '\N') AS decimal(20)),
            CAST(NULLIF(course_id, '\N') AS decimal(20)),
            CAST(NULLIF(group_weight, '\N') AS decimal(20))
     FROM canvas_load_assignment_group_fact);

-- Queries 1-C-8: Create and populate canvas_submission_dim
CREATE TABLE canvas_submission_dim
    (id decimal(20),
     canvas_id decimal(20),
     body varchar(4000),
     url varchar(4000),
     grade varchar(70),
     submitted_at timestamp,
     submission_type varchar(30),
     workflow_state varchar(20),
     created_at timestamp,
     updated_at timestamp,
     processed varchar(10),
     process_attempts decimal(20),
     grade_matches_current_submission varchar(10),
     published_grade varchar(70),
     graded_at timestamp,
     has_rubric_assessment varchar(10),
     attempt decimal(20),
     has_admin_comment varchar(10),
     assignment_id decimal(20),
     excused varchar(30),
     graded_anonymously varchar(30),
     grader_id decimal(20),
     group_id decimal(20),
     quiz_submission_id decimal(20),
     user_id decimal(20),
     grade_state varchar(20));

INSERT INTO canvas_submission_dim
    (SELECT CAST(NULLIF(id, '\N') AS decimal(20)),
            CAST(NULLIF(canvas_id, '\N') AS decimal(20)),
            NULLIF(body, '\N'),
            NULLIF(url, '\N'),
            NULLIF(grade, '\N'),
            TO_TIMESTAMP(NULLIF(submitted_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            NULLIF(submission_type, '\N'),
            NULLIF(workflow_state, '\N'),
            TO_TIMESTAMP(NULLIF(created_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            TO_TIMESTAMP(NULLIF(updated_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            NULLIF(processed, '\N'),
            CAST(NULLIF(process_attempts, '\N') AS decimal(20)),
            NULLIF(grade_matches_current_submission, '\N'),
            NULLIF(published_grade, '\N'),
            TO_TIMESTAMP(NULLIF(graded_at, '\N'), 'YYYY-MM-DD HH24:MI:SS.ff3'),
            NULLIF(has_rubric_assessment, '\N'),
            CAST(NULLIF(attempt, '\N') AS decimal(20)),
            NULLIF(has_admin_comment, '\N'),
            CAST(NULLIF(assignment_id, '\N') AS decimal(20)),
            NULLIF(excused, '\N'),
            NULLIF(graded_anonymously, '\N'),
            CAST(NULLIF(grader_id, '\N') AS decimal(20)),
            CAST(NULLIF(group_id, '\N') AS decimal(20)),
            CAST(NULLIF(quiz_submission_id, '\N') AS decimal(20)),
            CAST(NULLIF(user_id, '\N') AS decimal(20)),
            NULLIF(grade_state, '\N')
     FROM canvas_load_submission_dim);

-- Queries 1-C-9: Create and populate canvas_submission_fact
CREATE TABLE canvas_submission_fact
    (submission_id decimal(20),
     assignment_id decimal(20),
     course_id decimal(20),
     enrollment_term_id decimal(20),
     user_id decimal(20),
     grader_id decimal(20),
     course_account_id decimal(20),
     enrollment_rollup_id decimal(20),
     score decimal(20),
     published_score decimal(20),
     what_if_score decimal(20),
     submission_comments_count decimal(20),
     account_id decimal(20),
     assignment_group_id decimal(20),
     group_id decimal(20),
     quiz_id decimal(20),
     quiz_submission_id decimal(20),
     wiki_id decimal(20));

INSERT INTO canvas_submission_fact
    (SELECT CAST(NULLIF(submission_id, '\N') AS decimal(20)),
            CAST(NULLIF(assignment_id, '\N') AS decimal(20)),
            CAST(NULLIF(course_id, '\N') AS decimal(20)),
            CAST(NULLIF(enrollment_term_id, '\N') AS decimal(20)),
            CAST(NULLIF(user_id, '\N') AS decimal(20)),
            CAST(NULLIF(grader_id, '\N') AS decimal(20)),
            CAST(NULLIF(course_account_id, '\N') AS decimal(20)),
            CAST(NULLIF(enrollment_rollup_id, '\N') AS decimal(20)),
            CAST(NULLIF(score, '\N') AS decimal(20)),
            CAST(NULLIF(published_score, '\N') AS decimal(20)),
            CAST(NULLIF(what_if_score, '\N') AS decimal(20)),
            CAST(NULLIF(submission_comments_count, '\N') AS decimal(20)),
            CAST(NULLIF(account_id, '\N') AS decimal(20)),
            CAST(NULLIF(assignment_group_id, '\N') AS decimal(20)),
            CAST(NULLIF(group_id, '\N') AS decimal(20)),
            CAST(NULLIF(quiz_id, '\N') AS decimal(20)),
            CAST(NULLIF(quiz_submission_id, '\N') AS decimal(20)),
            CAST(NULLIF(wiki_id, '\N') AS decimal(20))
     FROM canvas_load_submission_fact);

-- Queries 1-C-10: Create and populate banner_gobsrid
CREATE TABLE banner_gobsrid
    (gobsrid_pidm decimal(8),
     gobsrid_sourced_id varchar(16));

INSERT INTO banner_gobsrid VALUES ('519353', '680204');
INSERT INTO banner_gobsrid VALUES ('546878', '428362');
INSERT INTO banner_gobsrid VALUES ('269787', '874390');
INSERT INTO banner_gobsrid VALUES ('752453', '315679');
INSERT INTO banner_gobsrid VALUES ('137588', '436280');
INSERT INTO banner_gobsrid VALUES ('215090', '122370');
INSERT INTO banner_gobsrid VALUES ('178294', '508968');
INSERT INTO banner_gobsrid VALUES ('159552', '168124');
INSERT INTO banner_gobsrid VALUES ('776269', '237272');
INSERT INTO banner_gobsrid VALUES ('694416', '829160');
INSERT INTO banner_gobsrid VALUES ('638159', '715744');
INSERT INTO banner_gobsrid VALUES ('434067', '655934');
INSERT INTO banner_gobsrid VALUES ('548987', '160478');
INSERT INTO banner_gobsrid VALUES ('185262', '257375');
INSERT INTO banner_gobsrid VALUES ('511449', '589513');
INSERT INTO banner_gobsrid VALUES ('983156', '416508');
INSERT INTO banner_gobsrid VALUES ('624369', '404105');
INSERT INTO banner_gobsrid VALUES ('662700', '666359');
INSERT INTO banner_gobsrid VALUES ('558146', '302475');
INSERT INTO banner_gobsrid VALUES ('793005', '392119');
INSERT INTO banner_gobsrid VALUES ('248168', '174471');
INSERT INTO banner_gobsrid VALUES ('147704', '630487');
INSERT INTO banner_gobsrid VALUES ('980097', '177125');
INSERT INTO banner_gobsrid VALUES ('249358', '599523');
INSERT INTO banner_gobsrid VALUES ('504093', '546679');
INSERT INTO banner_gobsrid VALUES ('972806', '848541');
INSERT INTO banner_gobsrid VALUES ('620401', '144402');
INSERT INTO banner_gobsrid VALUES ('540514', '666255');
INSERT INTO banner_gobsrid VALUES ('347288', '490376');
INSERT INTO banner_gobsrid VALUES ('250998', '161945');
INSERT INTO banner_gobsrid VALUES ('908154', '491256');
INSERT INTO banner_gobsrid VALUES ('139291', '917185');
INSERT INTO banner_gobsrid VALUES ('237328', '133718');
INSERT INTO banner_gobsrid VALUES ('831225', '101416');
INSERT INTO banner_gobsrid VALUES ('963562', '959905');
INSERT INTO banner_gobsrid VALUES ('599321', '246616');
INSERT INTO banner_gobsrid VALUES ('360227', '900712');
INSERT INTO banner_gobsrid VALUES ('902169', '278170');
INSERT INTO banner_gobsrid VALUES ('559531', '334981');
INSERT INTO banner_gobsrid VALUES ('523397', '559829');
INSERT INTO banner_gobsrid VALUES ('368243', '499908');
INSERT INTO banner_gobsrid VALUES ('754974', '961522');
INSERT INTO banner_gobsrid VALUES ('470456', '711716');
INSERT INTO banner_gobsrid VALUES ('600629', '635289');
INSERT INTO banner_gobsrid VALUES ('470899', '746645');
INSERT INTO banner_gobsrid VALUES ('645969', '320970');
INSERT INTO banner_gobsrid VALUES ('329228', '986617');
INSERT INTO banner_gobsrid VALUES ('315061', '360576');
INSERT INTO banner_gobsrid VALUES ('150722', '128722');
INSERT INTO banner_gobsrid VALUES ('212694', '550095');
INSERT INTO banner_gobsrid VALUES ('391642', '907329');
INSERT INTO banner_gobsrid VALUES ('220550', '814106');
INSERT INTO banner_gobsrid VALUES ('326782', '283982');
INSERT INTO banner_gobsrid VALUES ('936100', '812817');
INSERT INTO banner_gobsrid VALUES ('154682', '391543');
INSERT INTO banner_gobsrid VALUES ('828952', '859171');
INSERT INTO banner_gobsrid VALUES ('314061', '876301');
INSERT INTO banner_gobsrid VALUES ('998727', '384302');
INSERT INTO banner_gobsrid VALUES ('467393', '405710');
INSERT INTO banner_gobsrid VALUES ('310713', '386915');
INSERT INTO banner_gobsrid VALUES ('496740', '652938');
INSERT INTO banner_gobsrid VALUES ('748185', '104220');
INSERT INTO banner_gobsrid VALUES ('862308', '988821');
INSERT INTO banner_gobsrid VALUES ('484009', '382705');
INSERT INTO banner_gobsrid VALUES ('211604', '660786');

-- Queries 1-C-11: Create and populate banner_goremal
CREATE TABLE banner_goremal
    (goremal_pidm decimal(8),
     goremal_email_address varchar(128));

INSERT INTO banner_goremal VALUES ('519353', 'aarols28@my.school.edu');
INSERT INTO banner_goremal VALUES ('546878', 'johwri56@my.school.edu');
INSERT INTO banner_goremal VALUES ('269787', 'kevjon36@my.school.edu');
INSERT INTO banner_goremal VALUES ('752453', 'pautho49@my.school.edu');
INSERT INTO banner_goremal VALUES ('137588', 'jussan82@my.school.edu');
INSERT INTO banner_goremal VALUES ('215090', 'emisco77@my.school.edu');
INSERT INTO banner_goremal VALUES ('178294', 'meldav87@my.school.edu');
INSERT INTO banner_goremal VALUES ('159552', 'lislop66@my.school.edu');
INSERT INTO banner_goremal VALUES ('776269', 'garrob86@my.school.edu');
INSERT INTO banner_goremal VALUES ('694416', 'ashrod95@my.school.edu');
INSERT INTO banner_goremal VALUES ('638159', 'kimeva61@my.school.edu');
INSERT INTO banner_goremal VALUES ('434067', 'megyou97@my.school.edu');
INSERT INTO banner_goremal VALUES ('548987', 'jenpar18@my.school.edu');
INSERT INTO banner_goremal VALUES ('185262', 'eriras42@my.school.edu');
INSERT INTO banner_goremal VALUES ('511449', 'kensmi62@my.school.edu');
INSERT INTO banner_goremal VALUES ('983156', 'jornie28@my.school.edu');
INSERT INTO banner_goremal VALUES ('624369', 'andhar29@my.school.edu');
INSERT INTO banner_goremal VALUES ('662700', 'laupet71@my.school.edu');
INSERT INTO banner_goremal VALUES ('558146', 'robgar43@my.school.edu');
INSERT INTO banner_goremal VALUES ('793005', 'jesbro89@my.school.edu');
INSERT INTO banner_goremal VALUES ('248168', 'antlee67@my.school.edu');
INSERT INTO banner_goremal VALUES ('147704', 'amytur12@my.school.edu');
INSERT INTO banner_goremal VALUES ('980097', 'miccar11@my.school.edu');
INSERT INTO banner_goremal VALUES ('249358', 'karngu99@my.school.edu');
INSERT INTO banner_goremal VALUES ('504093', 'jarkin70@my.school.edu');
INSERT INTO banner_goremal VALUES ('972806', 'mathal83@my.school.edu');
INSERT INTO banner_goremal VALUES ('620401', 'amagre95@my.school.edu');
INSERT INTO banner_goremal VALUES ('540514', 'joscoo10@my.school.edu');
INSERT INTO banner_goremal VALUES ('347288', 'jacjoh60@my.school.edu');
INSERT INTO banner_goremal VALUES ('250998', 'jefbak78@my.school.edu');
INSERT INTO banner_goremal VALUES ('908154', 'jultay95@my.school.edu');
INSERT INTO banner_goremal VALUES ('139291', 'nicmar39@my.school.edu');
INSERT INTO banner_goremal VALUES ('237328', 'sarwal78@my.school.edu');
INSERT INTO banner_goremal VALUES ('831225', 'dannel30@my.school.edu');
INSERT INTO banner_goremal VALUES ('963562', 'jonnie32@my.school.edu');
INSERT INTO banner_goremal VALUES ('599321', 'natsor23@my.school.edu');
INSERT INTO banner_goremal VALUES ('360227', 'jerlar37@my.school.edu');
INSERT INTO banner_goremal VALUES ('902169', 'marcla98@my.school.edu');
INSERT INTO banner_goremal VALUES ('559531', 'jaspet31@my.school.edu');
INSERT INTO banner_goremal VALUES ('523397', 'heaste72@my.school.edu');
INSERT INTO banner_goremal VALUES ('368243', 'ricall65@my.school.edu');
INSERT INTO banner_goremal VALUES ('754974', 'bramit87@my.school.edu');
INSERT INTO banner_goremal VALUES ('470456', 'microb45@my.school.edu');
INSERT INTO banner_goremal VALUES ('600629', 'chamoo40@my.school.edu');
INSERT INTO banner_goremal VALUES ('470899', 'stejen75@my.school.edu');
INSERT INTO banner_goremal VALUES ('645969', 'wilhil20@my.school.edu');
INSERT INTO banner_goremal VALUES ('329228', 'adawoo38@my.school.edu');
INSERT INTO banner_goremal VALUES ('315061', 'reband60@my.school.edu');
INSERT INTO banner_goremal VALUES ('150722', 'joscam20@my.school.edu');
INSERT INTO banner_goremal VALUES ('212694', 'jamlew69@my.school.edu');
INSERT INTO banner_goremal VALUES ('391642', 'chrada97@my.school.edu');
INSERT INTO banner_goremal VALUES ('220550', 'ryamil32@my.school.edu');
INSERT INTO banner_goremal VALUES ('326782', 'thojac85@my.school.edu');
INSERT INTO banner_goremal VALUES ('936100', 'angher77@my.school.edu');
INSERT INTO banner_goremal VALUES ('154682', 'scowil50@my.school.edu');
INSERT INTO banner_goremal VALUES ('828952', 'brigar24@my.school.edu');
INSERT INTO banner_goremal VALUES ('314061', 'marwhi55@my.school.edu');
INSERT INTO banner_goremal VALUES ('998727', 'bengon54@my.school.edu');
INSERT INTO banner_goremal VALUES ('467393', 'tylall65@my.school.edu');
INSERT INTO banner_goremal VALUES ('310713', 'stechr71@my.school.edu');
INSERT INTO banner_goremal VALUES ('496740', 'nichan41@my.school.edu');
INSERT INTO banner_goremal VALUES ('748185', 'racbla36@my.school.edu');
INSERT INTO banner_goremal VALUES ('862308', 'davmar40@my.school.edu');
INSERT INTO banner_goremal VALUES ('484009', 'suswil45@my.school.edu');
INSERT INTO banner_goremal VALUES ('211604', 'elitho85@my.school.edu');

-- Queries 1-C-12: Create and populate banner_ssbsect
CREATE TABLE banner_ssbsect
    (ssbsect_term_code varchar(6),
     ssbsect_crn varchar(5),
     ssbsect_subj_code varchar(4),
     ssbsect_crse_numb varchar(5),
     ssbsect_seq_numb varchar(3));

INSERT INTO banner_ssbsect VALUES ('201920', '80141', 'ENGL', '101', '01');
INSERT INTO banner_ssbsect VALUES ('201920', '57815', 'ENGL', '201', '01');
INSERT INTO banner_ssbsect VALUES ('201920', '61490', 'MATH', '101', '01');
INSERT INTO banner_ssbsect VALUES ('201920', '25487', 'MATH', '201', '01');
INSERT INTO banner_ssbsect VALUES ('201920', '25763', 'HIST', '101', '01');
INSERT INTO banner_ssbsect VALUES ('201920', '77786', 'ENGL', '101', '02');
INSERT INTO banner_ssbsect VALUES ('201920', '82239', 'ENGL', '201', '02');
INSERT INTO banner_ssbsect VALUES ('201920', '94456', 'MATH', '101', '02');
INSERT INTO banner_ssbsect VALUES ('201920', '30084', 'MATH', '201', '02');
INSERT INTO banner_ssbsect VALUES ('201920', '63064', 'HIST', '101', '02');
INSERT INTO banner_ssbsect VALUES ('201920', '95507', 'ENGL', '101', '03');
INSERT INTO banner_ssbsect VALUES ('201920', '19183', 'ENGL', '201', '03');
INSERT INTO banner_ssbsect VALUES ('201920', '53455', 'MATH', '101', '03');
INSERT INTO banner_ssbsect VALUES ('201920', '44740', 'MATH', '201', '03');
INSERT INTO banner_ssbsect VALUES ('201920', '90118', 'HIST', '101', '03');

-- Queries 1-C-13: Create and populate banner_sfrstcr
CREATE TABLE banner_sfrstcr
    (sfrstcr_term_code varchar(6),
     sfrstcr_pidm decimal(8),
     sfrstcr_crn varchar(5),
     sfrstcr_rsts_code varchar(2),
     sfrstcr_grde_code varchar(6));

INSERT INTO banner_sfrstcr VALUES ('201920', '519353', '82239', 'RW', 'E');
INSERT INTO banner_sfrstcr VALUES ('201920', '519353', '63064', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '519353', '94456', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '546878', '77786', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '546878', '19183', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '752453', '77786', 'RW', 'E');
INSERT INTO banner_sfrstcr VALUES ('201920', '752453', '57815', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '137588', '95507', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '137588', '53455', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '215090', '61490', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '178294', '77786', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '159552', '95507', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '776269', '80141', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '776269', '19183', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '776269', '63064', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '694416', '25763', 'RW', 'E');
INSERT INTO banner_sfrstcr VALUES ('201920', '694416', '53455', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '638159', '94456', 'RW', 'C');
INSERT INTO banner_sfrstcr VALUES ('201920', '434067', '95507', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '434067', '25763', 'RW', 'E');
INSERT INTO banner_sfrstcr VALUES ('201920', '434067', '61490', 'RW', 'E');
INSERT INTO banner_sfrstcr VALUES ('201920', '434067', '25487', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '548987', '95507', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '548987', '63064', 'RW', 'C');
INSERT INTO banner_sfrstcr VALUES ('201920', '548987', '61490', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '185262', '77786', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '185262', '82239', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '511449', '80141', 'RW', 'E');
INSERT INTO banner_sfrstcr VALUES ('201920', '511449', '19183', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '511449', '25763', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '511449', '30084', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '983156', '80141', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '662700', '80141', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '662700', '53455', 'RW', 'C');
INSERT INTO banner_sfrstcr VALUES ('201920', '662700', '44740', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '558146', '95507', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '793005', '61490', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '248168', '95507', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '248168', '90118', 'RW', 'C');
INSERT INTO banner_sfrstcr VALUES ('201920', '248168', '94456', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '147704', '80141', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '147704', '44740', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '980097', '90118', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '980097', '94456', 'RW', 'E');
INSERT INTO banner_sfrstcr VALUES ('201920', '980097', '44740', 'RW', 'C');
INSERT INTO banner_sfrstcr VALUES ('201920', '249358', '82239', 'RW', 'D');
INSERT INTO banner_sfrstcr VALUES ('201920', '504093', '25763', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '504093', '53455', 'RW', 'D');
INSERT INTO banner_sfrstcr VALUES ('201920', '972806', '80141', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '972806', '90118', 'RW', 'D');
INSERT INTO banner_sfrstcr VALUES ('201920', '620401', '82239', 'RW', 'C');
INSERT INTO banner_sfrstcr VALUES ('201920', '540514', '57815', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '540514', '61490', 'RW', 'E');
INSERT INTO banner_sfrstcr VALUES ('201920', '347288', '94456', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '347288', '25487', 'RW', 'C');
INSERT INTO banner_sfrstcr VALUES ('201920', '250998', '19183', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '250998', '61490', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '250998', '25487', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '908154', '80141', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '908154', '82239', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '139291', '19183', 'RW', 'E');
INSERT INTO banner_sfrstcr VALUES ('201920', '139291', '30084', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '237328', '95507', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '237328', '94456', 'RW', 'D');
INSERT INTO banner_sfrstcr VALUES ('201920', '831225', '80141', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '831225', '61490', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '831225', '30084', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '963562', '19183', 'RW', 'E');
INSERT INTO banner_sfrstcr VALUES ('201920', '599321', '53455', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '360227', '25763', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '360227', '30084', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '902169', '61490', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '559531', '57815', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '559531', '25763', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '559531', '94456', 'RW', 'E');
INSERT INTO banner_sfrstcr VALUES ('201920', '559531', '30084', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '523397', '63064', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '523397', '44740', 'RW', 'E');
INSERT INTO banner_sfrstcr VALUES ('201920', '368243', '57815', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '368243', '90118', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '368243', '30084', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '754974', '77786', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '754974', '63064', 'RW', 'C');
INSERT INTO banner_sfrstcr VALUES ('201920', '754974', '44740', 'RW', 'E');
INSERT INTO banner_sfrstcr VALUES ('201920', '470456', '63064', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '470456', '25487', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '600629', '77786', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '600629', '19183', 'RW', 'C');
INSERT INTO banner_sfrstcr VALUES ('201920', '600629', '94456', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '600629', '25487', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '470899', '77786', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '470899', '90118', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '645969', '25763', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '329228', '63064', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '329228', '61490', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '329228', '44740', 'RW', 'A');
INSERT INTO banner_sfrstcr VALUES ('201920', '315061', '57815', 'RW', 'E');
INSERT INTO banner_sfrstcr VALUES ('201920', '150722', '57815', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '150722', '63064', 'RW', 'B');
INSERT INTO banner_sfrstcr VALUES ('201920', '150722', '61490', 'RW', 'C');
INSERT INTO banner_sfrstcr VALUES ('201920', '212694', '77786', 'RW', 'E');

