--------------------------------------------------------------------------------
-- Queries to query the Canvas data in the new star schemas
--------------------------------------------------------------------------------

-- Query 2-B-1: What are the final exam scores of all students in MATH 101?
SELECT warehouse_dim_section.course_subj, warehouse_dim_section.course_num,
       warehouse_dim_section.section_num,
       warehouse_dim_instructor.first_name || ' ' || warehouse_dim_instructor.last_name AS instructor_name,
       warehouse_dim_student.pidm,
       warehouse_dim_student.first_name || ' ' || warehouse_dim_student.last_name AS student_name,
       warehouse_dim_assignment.title, warehouse_fact_assgn_submissions.grade
FROM warehouse_fact_assgn_submissions
     JOIN warehouse_dim_assignment
     ON warehouse_fact_assgn_submissions.dim_assignment_key = warehouse_dim_assignment.dim_assignment_key
     JOIN warehouse_dim_student
     ON warehouse_fact_assgn_submissions.dim_student_key = warehouse_dim_student.dim_student_key
     JOIN warehouse_dim_section
     ON warehouse_fact_assgn_submissions.dim_section_key = warehouse_dim_section.dim_section_key
     JOIN warehouse_dim_instructor
     ON warehouse_fact_assgn_submissions.dim_instructor_key = warehouse_dim_instructor.dim_instructor_key
WHERE warehouse_dim_section.course_subj = 'MATH'
      AND warehouse_dim_section.course_num = '101'
      AND warehouse_dim_assignment.TITLE = 'Assignment  6'
ORDER BY warehouse_dim_section.SECTION_NUM, warehouse_dim_student.last_name;

-- Query 2-B-2: Are first-generation students any more (or less) likely to
-- submit assignments?
SELECT warehouse_dim_student.first_gen,
       ROUND(SUM(CASE WHEN date_submitted IS NOT NULL THEN 1 ELSE 0 END) / COUNT(*) * 100, 2) AS PCT_SUBMITTED
FROM warehouse_fact_assgn_submissions
     JOIN warehouse_dim_student
     ON warehouse_fact_assgn_submissions.dim_student_key = warehouse_dim_student.dim_student_key
GROUP BY warehouse_dim_student.first_gen;