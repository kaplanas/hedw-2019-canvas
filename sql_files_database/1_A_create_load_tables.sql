--------------------------------------------------------------------------------
-- Queries to create load tables for raw Canvas data
--------------------------------------------------------------------------------

-- Query 1-A-1: Create canvas_load_pseudonym_dim
CREATE TABLE canvas_load_pseudonym_dim
    (id varchar(14),
     canvas_id varchar(20),
     user_id varchar(19),
     account_id varchar(14),
     workflow_state varchar(64),
     last_request_at varchar(64),
     last_login_at varchar(64),
     current_login_at varchar(64),
     last_login_ip varchar(64),
     current_login_ip varchar(64),
     position varchar(20),
     created_at varchar(64),
     updated_at varchar(64),
     password_auto_generated varchar(64),
     deleted_at varchar(64),
     sis_user_id varchar(64),
     unique_name varchar(64),
     integration_id varchar(64),
     authentication_provider_id varchar(64));

-- Query 1-A-2: Create canvas_load_user_dim
CREATE TABLE canvas_load_user_dim
    (id varchar(19),
     canvas_id varchar(20),
     root_account_id varchar(14),
     name varchar(64),
     time_zone varchar(64),
     created_at varchar(64),
     visibility varchar(64),
     school_name varchar(64),
     school_position varchar(64),
     gender varchar(64),
     locale varchar(64),
     is_public varchar(64),
     birthdate varchar(64),
     country_code varchar(64),
     workflow_state varchar(64),
     sortable_name varchar(64),
     global_canvas_id varchar(15));

-- Query 1-A-3: Create canvas_load_course_dim
CREATE TABLE canvas_load_course_dim
    (id varchar(14),
     canvas_id varchar(20),
     root_account_id varchar(14),
     account_id varchar(14),
     enrollment_term_id varchar(14),
     name varchar(100),
     code varchar(100),
     type varchar(10),
     created_at varchar(50),
     start_at varchar(64),
     conclude_at varchar(64),
     publicly_visible varchar(30),
     sis_source_id varchar(2000),
     workflow_state varchar(64),
     wiki_id varchar(64),
     syllabus_body varchar(4000));

-- Query 1-A-4: Create canvas_load_course_section_dim
CREATE TABLE canvas_load_course_section_dim
    (id varchar(14),
     canvas_id varchar(20),
     name varchar(100),
     course_id varchar(14),
     enrollment_term_id varchar(64),
     default_section varchar(64),
     accepting_enrollments varchar(64),
     can_manually_enroll varchar(64),
     start_at varchar(64),
     end_at varchar(64),
     created_at varchar(50),
     updated_at varchar(64),
     workflow_state varchar(64),
     restrict_enrollments_to_section_dates varchar(64),
     nonxlist_course_id varchar(64),
     sis_source_id varchar(64));

-- Query 1-A-5: Create canvas_load_assignment_dim
CREATE TABLE canvas_load_assignment_dim
    (id varchar(14),
     canvas_id varchar(20),
     course_id varchar(14),
     title varchar(4000),
     description varchar(4000),
     due_at varchar(64),
     unlock_at varchar(64),
     lock_at varchar(64),
     points_possible varchar(64),
     grading_type varchar(64),
     submission_types varchar(64),
     workflow_state varchar(64),
     created_at varchar(64),
     updated_at varchar(64),
     peer_review_count varchar(64),
     peer_reviews_due_at varchar(64),
     peer_reviews_assigned varchar(10),
     peer_reviews varchar(64),
     automatic_peer_reviews varchar(10),
     all_day varchar(10),
     all_day_date varchar(64),
     could_be_locked varchar(10),
     grade_group_students_individually varchar(10),
     anonymous_peer_reviews varchar(10),
     muted varchar(14),
     assignment_group_id varchar(14),
     position varchar(64),
     visibility varchar(64),
     external_tool_id varchar(64));

-- Query 1-A-6: Create canvas_load_assignment_group_dim
CREATE TABLE canvas_load_assignment_group_dim
    (id varchar(30),
     canvas_id varchar(30),
     course_id varchar(30),
     name varchar(4000),
     default_assignment_name varchar(4000),
     workflow_state varchar(20),
     position varchar(20),
     created_at varchar(50),
     updated_at varchar(50));

-- Query 1-A-7: Create canvas_load_assignment_group_fact
CREATE TABLE canvas_load_assignment_group_fact
    (assignment_group_id varchar(64),
     course_id varchar(64),
     group_weight varchar(64));

-- Query 1-A-8: Create canvas_load_submission_dim
CREATE TABLE canvas_load_submission_dim
    (id varchar(14),
     canvas_id varchar(20),
     body varchar(4000),
     url varchar(4000),
     grade varchar(70),
     submitted_at varchar(64),
     submission_type varchar(64),
     workflow_state varchar(64),
     created_at varchar(64),
     updated_at varchar(64),
     processed varchar(64),
     process_attempts varchar(64),
     grade_matches_current_submission varchar(64),
     published_grade varchar(70),
     graded_at varchar(64),
     has_rubric_assessment varchar(64),
     attempt varchar(64),
     has_admin_comment varchar(20),
     assignment_id varchar(14),
     excused varchar(64),
     graded_anonymously varchar(64),
     grader_id varchar(64),
     group_id varchar(64),
     quiz_submission_id varchar(64),
     user_id varchar(19),
     grade_state varchar(64));

-- Query 1-A-9: Create canvas_load_submission_fact
CREATE TABLE canvas_load_submission_fact
    (submission_id varchar(30),
     assignment_id varchar(30),
     course_id varchar(30),
     enrollment_term_id varchar(30),
     user_id varchar(30),
     grader_id varchar(30),
     course_account_id varchar(30),
     enrollment_rollup_id varchar(30),
     score varchar(64),
     published_score varchar(64),
     what_if_score varchar(64),
     submission_comments_count varchar(30),
     account_id varchar(30),
     assignment_group_id varchar(30),
     group_id varchar(30),
     quiz_id varchar(30),
     quiz_submission_id varchar(30),
     wiki_id varchar(30));

