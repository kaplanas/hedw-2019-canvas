--------------------------------------------------------------------------------
-- Queries to create new dims from Canvas data
--------------------------------------------------------------------------------

-- Queries 2-A-1: Create dim_assignment
CREATE SEQUENCE warehouse_dim_assignment_seq
    START WITH 1
    INCREMENT BY 1;
CREATE TABLE warehouse_dim_assignment AS
    (SELECT -- Use the sequence to assign the dimension key.
            warehouse_dim_assignment_seq.NEXTVAL AS dim_assignment_key,
            -- Get the Canvas assignment and course IDs; these are our business
            -- keys.
            canvas_assignment_dim.canvas_id,
            canvas_assignment_dim.id AS canvas_internal_id,
            canvas_assignment_dim.course_id AS canvas_course_id,
            -- Title of the assignment.
            canvas_assignment_dim.title,
            -- Created date and due date of the assignment.
            canvas_assignment_dim.created_at AS date_created,
            canvas_assignment_dim.due_at AS date_due,
            -- How many points is the assignment worth?
            canvas_assignment_dim.points_possible,
            -- ROUGHLY what percent of the student's overall grade is the
            -- assignment worth?
            (canvas_assignment_dim.points_possible /
             SUM(canvas_assignment_dim.points_possible)
             OVER (PARTITION BY canvas_assignment_dim.assignment_group_id)) *
            (canvas_assignment_group_fact.group_weight /
             canvas_assignment_group_fact.total_group_weights) * 100 AS percent_worth,
            -- What kind of grade does this assignment receive?  (points,
            -- complete/incomplete, etc.)
            canvas_assignment_dim.grading_type,
            -- How do students submit this assignment?
            canvas_assignment_dim.submission_types,
            -- Has this assignment been published?
            canvas_assignment_dim.workflow_state,
            -- What group is this assignment in?
            canvas_assignment_group_dim.id AS canvas_assignment_group_id,
            canvas_assignment_group_dim.name AS assignment_group
     FROM -- Assignment-level fields.
          canvas_assignment_dim
          -- Join to assignment_group_dim to get the name of the group.
          JOIN canvas_assignment_group_dim
          ON canvas_assignment_dim.ASSIGNMENT_GROUP_ID = canvas_assignment_group_dim.ID
          -- From assignment_group_fact, get the weight of each group and the
          -- total of all group weights in the course (NOT always 100% in real
          -- Canvas data).
          JOIN (SELECT assignment_group_id, group_weight,
                       SUM(group_weight)
                       OVER (PARTITION BY course_id) AS total_group_weights
                FROM canvas_assignment_group_fact) canvas_assignment_group_fact
          ON canvas_assignment_dim.ASSIGNMENT_GROUP_ID = canvas_assignment_group_fact.ASSIGNMENT_GROUP_ID);

-- Query 2-A-2: Create fact_assgn_submissions
CREATE TABLE warehouse_fact_assgn_submissions AS
    (SELECT -- Get dimension keys.
            warehouse_dim_student.dim_student_key,
            warehouse_dim_instructor.dim_instructor_key,
            warehouse_dim_assignment.dim_assignment_key,
            warehouse_dim_section.dim_section_key,
            -- Has this submission been submitted/graded yet?  When?
            canvas_submission_dim.workflow_state,
            canvas_submission_dim.submitted_at as date_submitted,
            canvas_submission_dim.graded_at as date_graded,
            -- Grade of the submission.
            canvas_submission_dim.grade,
            canvas_submission_fact.score
     FROM -- Some submission-level fields.
          canvas_submission_dim
          -- More submission-level fields.
          JOIN canvas_submission_fact
          ON canvas_submission_dim.id = canvas_submission_fact.submission_id
          -- Get the student via USER_ID -> pseudonym_dim -> gobsrid ->
          -- dim_student.
          JOIN canvas_pseudonym_dim pseudonym_student
          ON canvas_submission_dim.user_id = pseudonym_student.user_id
          JOIN banner_gobsrid gobsrid_student
          ON pseudonym_student.sis_user_id = gobsrid_student.gobsrid_sourced_id
          JOIN warehouse_dim_student
          ON gobsrid_student.gobsrid_pidm = warehouse_dim_student.pidm
          -- Get the grader via GRADER_ID -> pseudonym_dim -> gobsrid ->
          -- dim_instructor.
          JOIN canvas_pseudonym_dim pseudonym_instructor
          ON canvas_submission_dim.grader_id = pseudonym_instructor.user_id
          JOIN banner_gobsrid gobsrid_instructor
          ON pseudonym_instructor.sis_user_id = gobsrid_instructor.gobsrid_sourced_id
          JOIN warehouse_dim_instructor
          ON gobsrid_instructor.gobsrid_pidm = warehouse_dim_instructor.pidm
          -- Get the course section.
          JOIN canvas_course_section_dim
          ON canvas_submission_fact.course_id = canvas_course_section_dim.course_id
          JOIN banner_sfrstcr
          ON SUBSTR(canvas_course_section_dim.sis_source_id, 1, 5) = banner_sfrstcr.sfrstcr_crn
             AND SUBSTR(canvas_course_section_dim.sis_source_id, 7, 12) = banner_sfrstcr.sfrstcr_term_code
             AND warehouse_dim_student.pidm = banner_sfrstcr.sfrstcr_pidm
          JOIN warehouse_dim_section
          ON banner_sfrstcr.sfrstcr_crn = warehouse_dim_section.crn
             AND banner_sfrstcr.sfrstcr_term_code = warehouse_dim_section.term_code
          -- Get the assignment.
          JOIN warehouse_dim_assignment
          ON canvas_submission_dim.assignment_id = warehouse_dim_assignment.canvas_internal_id);