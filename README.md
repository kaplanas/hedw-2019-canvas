# hedw-2019-canvas

Materials for "Integrating Canvas into Your Data Warehouse" presentations at HEDW 2019

SQL Fiddle: https://www.db-fiddle.com/f/6LzcJH4pbK4kSt4C1eSeeQ/6

**Please click "Fork" before you change anything in the fiddle.**

## Dummy data files

The `dummy_files` folder contains fake data files for nine Canvas tables:

- `assignment_dim`
- `assignment_group_dim`
- `assignment_group_fact`
- `course_dim`
- `course_section_dim`
- `pseudonym_dim`
- `submission_dim`
- `submission_fact`
- `user_dim`

The data is fake, but otherwise as realistic as possible.  The formatting closely matches actual raw data files from Canvas.

## SQL files

There are two folders with SQL: one that contains all the SQL pasted into the fiddle (`sql_files_fiddle`), and one with SQL that can be run in an Oracle database (`sql_files_database`).  Contents are as follows:

- `1_A_create_load_tables`: `CREATE` statements, one for each Canvas table.  Correct data types for the fiddle; all strings for Oracle.
- `1_B_populate_load_tables`: `INSERT` statements that populate the load tables with Canvas data.  Fiddle only; if you have an Oracle sandbox, upload the dummy files into your load tables.
- `1_C_create_and_populate_data_stores`: `CREATE` statements that populate data stores out of the Canvas load tables.  The processing converts data types and deals with NULLs.  Oracle only; the fiddle created everything with the correct data types in the first place.
- `1_D_create_old_dims`: `CREATE` and `INSERT` statements for three tables you might already have in a higher ed data warehouse: `dim_student`, `dim_section`, and `dim_instructor`.
- `2_A_create_new_dims`: `CREATE` statements for two new tables, `assignment_dim` and `fact_assgn_submissions`, that pull from Canvas data.
- `2_B_query_canvas_data`: `SELECT` statement that illustrate what we can do with the new Canvas star schema.
